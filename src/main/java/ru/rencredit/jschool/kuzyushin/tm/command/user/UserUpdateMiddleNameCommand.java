package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateMiddleNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-update-middle-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateMiddleName(id, middleName);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }
}
