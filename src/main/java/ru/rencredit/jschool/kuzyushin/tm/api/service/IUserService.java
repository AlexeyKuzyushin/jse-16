package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User updateLogin(String id, String login);

    User updatePassword(String id, String password);

    User updateEmail(String id, String email);

    User updateFirstName(String id, String firstName);

    User updateLastName(String id, String lastName);

    User updateMiddleName(String id, String middleName);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);
}
