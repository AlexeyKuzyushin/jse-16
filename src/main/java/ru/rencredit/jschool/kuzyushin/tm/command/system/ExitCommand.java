package ru.rencredit.jschool.kuzyushin.tm.command.system;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String name()  {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description()   {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
