package ru.rencredit.jschool.kuzyushin.tm.command.task;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().findALl(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }
}
