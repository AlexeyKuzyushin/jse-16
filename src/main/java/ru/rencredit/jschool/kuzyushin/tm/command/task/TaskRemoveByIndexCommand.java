package ru.rencredit.jschool.kuzyushin.tm.command.task;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }
}
