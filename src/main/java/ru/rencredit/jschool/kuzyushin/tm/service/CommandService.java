package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ICommandService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }
}
