package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-update-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PASSWORD]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updatePassword(id, password);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }
}
