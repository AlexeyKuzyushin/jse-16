package ru.rencredit.jschool.kuzyushin.tm.command.task;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
        if (task == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }
}
