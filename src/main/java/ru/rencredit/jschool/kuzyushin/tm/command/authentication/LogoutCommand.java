package ru.rencredit.jschool.kuzyushin.tm.command.authentication;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }
}
