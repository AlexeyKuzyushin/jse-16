package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) throw new EmptyUserException();
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User updateLogin(final String id, final String login) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        return user;
    }

    @Override
    public User updatePassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setPasswordHash(password);
        return user;
    }

    @Override
    public User updateEmail(final String id, final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateFirstName(final String id, final String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        return user;
    }

    @Override
    public User updateLastName(final String id, final String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateMiddleName(final String id, final String middleName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        return null;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        return null;
    }
}
